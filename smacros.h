#ifndef SMACROS_H
#define SMACROS_H

#define CALL_MEMBER_FUNCTION(object, functionPointer) ((object).*(functionPointer))

#endif // !SMACROS_H
