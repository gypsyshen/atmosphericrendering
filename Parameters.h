#pragma once

#ifndef PARAMETERS_H
#define PARAMETERS_H

class Ray2f;
class Point2f;

#include "GlobalConstants.h"

struct Parameters;

// Function Pointer

using AtmosphericDensityMemberFunction = float(Parameters::*)(float) const;
using InvScaleHeightMemberFunction = float(Parameters::*)() const;

struct Parameters
{
public:
	Parameters();
	~Parameters();

	float computeRayleighScatteringAtmosphericDensity(float normalizedAltitude) const;
	float computeMieScatteringAtmosphericDensity(float normalizedAltitude) const;

	float computeRayleighScatteringOpticalDepth(
		float normalizedAltitude, 
		float normalizedVerticalAngle) const;

	float computeMieScatteringOpticalDepth(
		float normalizedAltitude, 
		float normalizedVerticalAngle) const;

	float rayleighScatteringInvScaleHeight() const;

	float mieScatteringInvScaleHeight() const;

private:

	Ray2f normalizedRay(float normalizedAltitude, float normalizedVerticalAngle) const;
	Point2f computeIntersectionWithSky(const Ray2f& normalizedRay) const;
	float normalizedAltitude(const Ray2f& normalizedRay) const;

	float computeOpticalDepth(
		float normalizedAltitude,
		float normalizedVerticalAngle,
		AtmosphericDensityMemberFunction) const;

	float computeAtmosphericDensity(
		float normalizedAltitude,
		InvScaleHeightMemberFunction invScaleHeightMemberFunction) const;

	float _rayMarchingInterval;
};

#endif // !PARAMETERS_H
