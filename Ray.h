#ifndef RAY_H
#define RAY_H

#include "Point.h"
#include "Vector.h"

struct Ray2f {

	constexpr Ray2f();
	Ray2f(const Point2f& _p, const Vector2f& _dir);
	~Ray2f();

	void normalize();
	bool reached(const Point2f& point) const;
	void stepForward(float interval);

	Point2f p;
	Vector2f dir;
};

#endif // !SRAY2F_H
