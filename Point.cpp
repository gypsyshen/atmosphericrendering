#include "stdafx.h"
#include "Point.h"
#include "Vector.h"

// Construct & Destruct

constexpr
Point2f::Point2f() : x(0.0), y(0.0) { }

Point2f::Point2f(const Point2f& p) : x(p.x), y(p.y) { }

constexpr
Point2f::Point2f(float _x, float _y) : x(_x), y(_y) { }

Point2f::~Point2f() { }

// Move

void Point2f::move(const Vector2f& dir, float distance)
{
	// TEST: move current point with the given distance along the direction
	Vector2f normDir = dir.getNormalized();
	x += normDir.x * distance;
	y += normDir.y * distance;
}

Vector2f Point2f::operator-(const Point2f& q) const
{
	return Vector2f(x - q.x, y - q.y);
}

Point2f Point2f::operator + (const Vector2f& v) const
{
	return Point2f(x + v.x, y + v.y);
}
