#include "stdafx.h"
#include "GlobalConstants.h"

// Construct & Destruct

GlobalConstants::GlobalConstants() : _normalizedEarthRadius(GlobalConstants::ratioOfEarthRadiusToAtmosphericThickness * 1.0f)
{
	// Atmospheric thickenss: 1.0f
	_normalizedEarthRadius = GlobalConstants::ratioOfEarthRadiusToAtmosphericThickness * 1.0f;
	this->initAltitudesAndVerticalAngles();
}

GlobalConstants::~GlobalConstants() { }

// Public Methods

const vector<float> & GlobalConstants::normalizedAltitudes() const
{
	return _normalizedAltitudes;
}

const vector<float> & GlobalConstants::normalizedVerticalAngles() const
{
	return _normalizedVerticalAngles;
}

float GlobalConstants::normalizedEarthRadius() const
{
	return _normalizedEarthRadius;
}

float GlobalConstants::normalizedAtmosphereRadius() const
{
	return _normalizedEarthRadius + 1.f;
}

// Private Methods (Initialization)

void GlobalConstants::initAltitudesAndVerticalAngles()
{
	float minNormalizedAltitude = 0.f;
	float maxNormalizedAltitude = 1.f;
	float altitudeInterval = 0.01f; // TODO: revert to 0.01f after testing
	initList<float>(minNormalizedAltitude, maxNormalizedAltitude, altitudeInterval, _normalizedAltitudes);

	float minNormalizedVerticalAngle = 0.f;
	float maxNormalizedVerticalAngle = 0.5f;
	float verticalAngleInterval = 0.01f;
	initList<float>(minNormalizedVerticalAngle, maxNormalizedVerticalAngle, verticalAngleInterval, _normalizedVerticalAngles);
}

template<typename T>
void GlobalConstants::initList(const T& minValue, const T& maxValue, const T& interval, vector<T>& list) const
{
	for (T v = minValue; v < maxValue; v += interval)
	{
		list.push_back(v);
	}
}
