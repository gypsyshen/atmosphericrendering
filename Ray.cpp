#include "stdafx.h"
#include "Ray.h"

// Construct & Deconstruct

constexpr Ray2f::Ray2f() : p(), dir() { }

Ray2f::Ray2f(const Point2f& _p, const Vector2f& _dir) : p(_p), dir(_dir) { }

Ray2f::~Ray2f() { }

// Normalization

void Ray2f::normalize()
{
	dir.normalize();
}

// Check If Reached a Point

bool Ray2f::reached(const Point2f& point) const
{
	// TEST: check if the ray has reached a given point
	// @param point: a point in the direction of current ray

	Vector2f direction = point - p;
	if (direction.x <= 0.0 && direction.y <= 0.0)
	{
		return true;
	}

	float dot = direction * dir;
	
	return (dot < 0.0);
}

// Move the ray towards its own direction

void Ray2f::stepForward(float interval)
{
	p.move(dir, interval);
}
