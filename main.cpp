// AtmosphereRendering.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Parameters.h"
#include "GlobalConstants.h"
#include <fstream>
#include <string>
#include <sstream>
#include <thread>

using namespace std;

string filename(const string&);
string filename(const char *);

template<class T>
bool writeAxisData(const char * filenameString, const vector<T>& data);

void rayleighOpticalDepth();
void mieOpticalDepth();

int main()
{
	GlobalConstants gc;

	decltype(auto) normalizedAltitudes = gc.normalizedAltitudes();
	writeAxisData<float>("altitudes", normalizedAltitudes);

	decltype(auto) normalizedVerticalAngles = gc.normalizedVerticalAngles();
	writeAxisData<float>("vertical_angles", normalizedVerticalAngles);

	// Write rayleigth scattering optical depth
	thread rayleighTh(rayleighOpticalDepth);

	// Write mie scattering optical depth
	thread mieTh(mieOpticalDepth);

	rayleighTh.join();
	mieTh.join();

	cout << "\n\n\n ---------- Finished parameter calculation! ----------" << endl;

	while (1) {}

	return 0;
}

// Compute Rayleigh Optical Depth

void rayleighOpticalDepth()
{
	GlobalConstants globalConstants;
	decltype(auto) normalizedAltitudes = globalConstants.normalizedAltitudes();
	decltype(auto) normalizedVerticalAngles = globalConstants.normalizedVerticalAngles();

	Parameters skyRenderingParameters;

	// Write rayleigth scattering optical depth
	stringstream rayleighOutputFileStream;
	rayleighOutputFileStream << "rayleigh_optical_depth_scale_height_" << 1.f / skyRenderingParameters.rayleighScatteringInvScaleHeight();
	string rayleighOpticalDepthFileName(filename(rayleighOutputFileStream.str()));
	ofstream rayleighOpticalDepthFile;
	rayleighOpticalDepthFile.open(rayleighOpticalDepthFileName);

	cout << "Started writing rayleigh scattering optical depth to \"" << rayleighOpticalDepthFileName << "\"." << endl << endl;

	int count = 0;
	const int countAll = normalizedAltitudes.size() * normalizedVerticalAngles.size();

	cout << "Rayleigh optical depth computation: " << count << " / " << countAll << " finished..";

	for each (auto normalizedAltitude in normalizedAltitudes)
	{
		for (size_t indexOfNormalizedVerticalAngle = 0; indexOfNormalizedVerticalAngle < normalizedVerticalAngles.size(); indexOfNormalizedVerticalAngle++)
		{
			auto normalizedVerticalAngle = normalizedVerticalAngles.at(indexOfNormalizedVerticalAngle);
			bool endOfVerticalAngleLoop = indexOfNormalizedVerticalAngle == normalizedVerticalAngles.size() - 1;

			// Calculate optical depth
			float rayleighOpticalDepth = skyRenderingParameters.computeRayleighScatteringOpticalDepth(
				normalizedAltitude,
				normalizedVerticalAngle);

			if (rayleighOpticalDepthFile.is_open())
			{
				rayleighOpticalDepthFile << rayleighOpticalDepth << (endOfVerticalAngleLoop ? "\n" : ",");
			}
			else {
				cout << "rayleigh ERROR: failed to write to " << rayleighOpticalDepthFileName << ". " << rayleighOpticalDepthFileName << " isn't open." << endl << endl;
			}

			count++;

			cout << "\rRayleigh optical depth computation: " << count << " / " << countAll << " finished..";

		} // Vertical angle loop
	} // Altitude loop

	if (rayleighOpticalDepthFile.is_open())
	{
		rayleighOpticalDepthFile.close();
		cout << "rayleigh Finished writing rayleigh scattering optical depth to \"" << rayleighOpticalDepthFileName << "\"!" << endl << endl;
	}
}

// Compute Mie Optical Depth

void mieOpticalDepth() {

	GlobalConstants globalConstants;
	decltype(auto) normalizedAltitudes = globalConstants.normalizedAltitudes();
	decltype(auto) normalizedVerticalAngles = globalConstants.normalizedVerticalAngles();

	Parameters skyRenderingParameters;

	// Write mie scattering optical depth
	stringstream mieOutputFileStream;
	mieOutputFileStream << "mie_optical_depth_scale_height_" << 1.f / skyRenderingParameters.mieScatteringInvScaleHeight();
	string mieOpticalDepthFileName(filename(mieOutputFileStream.str()));
	ofstream mieOpticalDepthFile;
	mieOpticalDepthFile.open(mieOpticalDepthFileName);

	cout << "Started writing mie scattering optical depth to \"" << mieOpticalDepthFileName << "\"." << endl << endl;

	int count = 0;
	const int countAll = normalizedAltitudes.size() * normalizedVerticalAngles.size();

	cout << "Mie optical depth computation: " << count << " / " << countAll << " finished..";

	for each (auto normalizedAltitude in normalizedAltitudes)
	{
		for (size_t indexOfNormalizedVerticalAngle = 0; indexOfNormalizedVerticalAngle < normalizedVerticalAngles.size(); indexOfNormalizedVerticalAngle++)
		{
			auto normalizedVerticalAngle = normalizedVerticalAngles.at(indexOfNormalizedVerticalAngle);
			bool endOfVerticalAngleLoop = indexOfNormalizedVerticalAngle == normalizedVerticalAngles.size() - 1;

			// Calculate optical depth
			float mieOpticalDepth = skyRenderingParameters.computeMieScatteringOpticalDepth(
				normalizedAltitude,
				normalizedVerticalAngle);

			if (mieOpticalDepthFile.is_open())
			{
				mieOpticalDepthFile << mieOpticalDepth << (endOfVerticalAngleLoop ? "\n" : ",");
			}
			else {
				cout << "mie ERROR: failed to write to " << mieOpticalDepthFileName << ". " << mieOpticalDepthFileName << " isn't open." << endl << endl;
			}

			count++;
			cout << "\rMie optical depth computation: " << count << " / " << countAll << " finished..";

		} // Vertical angle loop
	} // Altitude loop

	if (mieOpticalDepthFile.is_open())
	{
		mieOpticalDepthFile.close();
		cout << "mie Finished writing mie scattering optical depth to " << mieOpticalDepthFileName << "\"!" << endl << endl;
	}
}

// Writing to Files

string filename(const string& name)
{
	return string("Data\\" + name + ".txt");
}

string filename(const char * name)
{
	return filename(string(name));
}

template<typename T>
bool writeAxisData(const char * filenameString, const vector<T>& data)
{
	ofstream file;
	string filenamePath = filename(filenameString);
	file.open(filenamePath);
	cout << "Started writing " << filenameString <<" to \"" << filenamePath << "\"." << endl << endl;
	for (size_t i = 0; i < data.size(); ++i)
	{
		auto value = data.at(i);
		bool endOfLoop = i == data.size() - 1;

		if (file.is_open())
		{
			file << value;
			if (!endOfLoop)
			{
				file << ",";
			}
		}
		else
		{
			cout << "Failed writing " << filenameString << " to \"" << filenamePath << "\"." << endl << endl;
			return false;
		}
	}
	if (file.is_open())
	{
		file.close();
		cout << "Finished writing " << filenameString << " to \"" << filenamePath << "\"!" << endl << endl;
	}
	else
	{
		cout << "Failed finishing writing to \"" << filenamePath << "\". The file isn't open." << endl << endl;
		return false;
	}

	return true;
}
