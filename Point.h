#ifndef POINT_H
#define POINT_H

struct Vector2f;

struct Point2f {

	constexpr Point2f();
	Point2f(const Point2f&);
	constexpr Point2f(float _x, float _y);
	~Point2f();

	void move(const Vector2f &dir, float distance);
	Vector2f operator - (const Point2f&) const;
	Point2f operator + (const Vector2f&) const;

	float x;
	float y;
};

#endif // !POINT_H
