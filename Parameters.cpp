#include "stdafx.h"
#include "Parameters.h"
#include "Vector.h"
#include "Ray.h"

// Construct and Destruct

Parameters::Parameters(): _rayMarchingInterval(0.01f) { }

Parameters::~Parameters() { }

// Atmospheric Density

float Parameters::computeRayleighScatteringAtmosphericDensity(float normalizedAltitude) const
{
	InvScaleHeightMemberFunction rayleighScatteringInvScaleHeightFunction = &Parameters::rayleighScatteringInvScaleHeight;
	float atmosphericDensity = computeAtmosphericDensity(normalizedAltitude, rayleighScatteringInvScaleHeightFunction);

	return atmosphericDensity;
}

float Parameters::computeMieScatteringAtmosphericDensity(float normalizedAltitude) const
{
	InvScaleHeightMemberFunction mieScatteringInvScaleHeightFunction = &Parameters::mieScatteringInvScaleHeight;
	float atmosphericDensity = computeAtmosphericDensity(normalizedAltitude, mieScatteringInvScaleHeightFunction);

	return atmosphericDensity;
}

float Parameters::computeAtmosphericDensity(
	float normalizedAltitude, 
	InvScaleHeightMemberFunction invScaleHeightMemberFunction) const
{
	float invScaleHeight = CALL_MEMBER_FUNCTION(*this, invScaleHeightMemberFunction)();
	float atmosphericDensity = exp(-normalizedAltitude * invScaleHeight);

	return atmosphericDensity;
}

// Optical Depth

float Parameters::computeRayleighScatteringOpticalDepth(
	float normalizedAltitude, 
	float normalizedVerticalAngle) const
{
	AtmosphericDensityMemberFunction rayleighScatteringAtmosphericDensityFunction = &Parameters::computeRayleighScatteringAtmosphericDensity;
	float result = this->computeOpticalDepth(
		normalizedAltitude,
		normalizedVerticalAngle,
		rayleighScatteringAtmosphericDensityFunction);

	return result;
}

float Parameters::computeMieScatteringOpticalDepth(
	float normalizedAltitude, 
	float normalizedVerticalAngle) const
{
	AtmosphericDensityMemberFunction mieScatteringAtmosphericDensityFunction = &Parameters::computeMieScatteringAtmosphericDensity;
	float result = this->computeOpticalDepth(
		normalizedAltitude, 
		normalizedVerticalAngle, 
		mieScatteringAtmosphericDensityFunction);

	return result;
}

float Parameters::computeOpticalDepth(
	float normalizedAltitude,
	float normalizedVerticalAngle,
	AtmosphericDensityMemberFunction atmosphericDensityMemberFunction) const
{
	Ray2f normalizedRay = this->normalizedRay(normalizedAltitude, normalizedVerticalAngle);
	Point2f outerAtmosphereIntersection = this->computeIntersectionWithSky(normalizedRay);
	float rayMarchingInterval = this->_rayMarchingInterval;

	Ray2f marchingRay(normalizedRay);
	float opticalDepth = 0.0f;
	while (!marchingRay.reached(outerAtmosphereIntersection))
	{
		float normalizedAltitude = this->normalizedAltitude(marchingRay);
		float atmosphericDensity = CALL_MEMBER_FUNCTION(*this, atmosphericDensityMemberFunction)(normalizedAltitude);
		opticalDepth += atmosphericDensity * rayMarchingInterval;
		marchingRay.stepForward(rayMarchingInterval);
	}

	return opticalDepth;
}

// Ray

/*
@param normalizedAltitude: [0, 1]
@param normalizedVerticalAngle: [0, 0.5]
*/
Ray2f Parameters::normalizedRay(float normalizedAltitude, float normalizedVerticalAngle) const
{
	// Starting point: normalizedAltitude
	// Direction: normalizedVerticalAngle
	GlobalConstants constants;
	Point2f p(0.0f, normalizedAltitude + constants.normalizedEarthRadius());

	float PI = 3.1415926f;
	float theta = normalizedVerticalAngle * PI;
	Vector2f dir(sinf(theta), cosf(theta));
	Ray2f ray(p, dir);
	ray.normalize();

	return ray;
}

Point2f Parameters::computeIntersectionWithSky(const Ray2f& normalizedRay) const
{
	Point2f p = normalizedRay.p;
	Vector2f dir = normalizedRay.dir;

	GlobalConstants constants;
	float R = constants.normalizedAtmosphereRadius();

	float A = dir.x * dir.x + dir.y * dir.y;
	float B = p.x * dir.x + p.y * dir.y;
	float C = p.x * p.x + p.y * p.y - R * R;

	float det = B * B - 4.0f * A * C;
	float t = 0.0;
	if (det > 0.0f)
	{
		float t0 = (-B + sqrt(det)) / (2.0f * A);
		float t1 = (-B - sqrt(det)) / (2.0f * A);
		t = t0 > t1 ? t0 : t1;
	}
	Point2f result = p + dir * t;

	return result;
}

float Parameters::normalizedAltitude(const Ray2f& normalizedRay) const
{
	Point2f p = normalizedRay.p;
	float distanceToEarthCenter = sqrt(p.x * p.x + p.y * p.y);
	GlobalConstants constants;
	float altitude = distanceToEarthCenter - constants.normalizedEarthRadius();
	return altitude;
}

float Parameters::rayleighScatteringInvScaleHeight() const
{
	// Reference: http://http.developer.nvidia.com/GPUGems2/gpugems2_chapter16.html
	// H_0 is the scale height, which is the height at which the atmosphere's average density is found. 
	// This implementation uses 0.25, so the average density is found 25 percent of the way up from the ground to the sky dome.

	// Scale height: 0.25f. Inv: 1/0.25f = 4.f
	return 4.f;
}

float Parameters::mieScatteringInvScaleHeight() const
{
	// Reference: http://http.developer.nvidia.com/GPUGems2/gpugems2_chapter16.html
	// H_0 is the scale height, which is the height at which the atmosphere's average density is found. 
	// This implementation uses 0.1, so the average density is found 25 percent of the way up from the ground to the sky dome.
	
	// Scale height: 0.1f. Inv: 1/0.1f = 10.f
	return 10.f;
}
