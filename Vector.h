#ifndef VECTOR_H
#define VECTOR_H

struct Vector2f {

	constexpr Vector2f();
	Vector2f(const Vector2f&);
	constexpr Vector2f(float _x, float _y);
	~Vector2f();

	void normalize();
	Vector2f getNormalized() const;

	float length() const;

	// Dot Product
	float operator*(const Vector2f&) const noexcept;

	// Multiplication
	Vector2f operator*(float) const noexcept;

	float x;
	float y;
};

#endif // !SVECTOR2F_H
