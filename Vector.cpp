#include "stdafx.h"
#include "Vector.h"

using namespace std;

// Construct & Destruct

constexpr
Vector2f::Vector2f(): x(0.0), y(0.0) { }

Vector2f::Vector2f(const Vector2f& v): x(v.x), y(v.y) { }

constexpr
Vector2f::Vector2f(float _x, float _y): x(_x), y(_y) { }

Vector2f::~Vector2f() { }

// Normalization

void Vector2f::normalize()
{
	float length = this->length();
	if (length == 0.0) {
		cout << "Normalization failed. Invalid vector length." << endl;
		return;
	}
	x /= length;
	y /= length;
}

Vector2f Vector2f::getNormalized() const
{
	Vector2f v(*this);
	v.normalize();

	return v;
}

float Vector2f::length() const
{
	return sqrtf(x * x + y * y);
}

// Operator overloading

// Dot Product
float Vector2f::operator*(const Vector2f& v) const noexcept
{
	return x * v.x + y * v.y;
}

// Multiplication
Vector2f Vector2f::operator*(float t) const noexcept
{
	return Vector2f(x * t, y * t);
}
