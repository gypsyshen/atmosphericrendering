#ifndef GLOBALCONSTANTS_H
#define GLOBALCONSTANTS_H

#include <vector>

using namespace std;

struct GlobalConstants
{
public:
	GlobalConstants();
	~GlobalConstants();
	
	const vector<float> & normalizedAltitudes() const;
	const vector<float> & normalizedVerticalAngles() const;

	// Normalized earth radius is a value in coordinate with atmospheric thickness of range 0 to 1
	float normalizedEarthRadius() const;
	float normalizedAtmosphereRadius() const;

private:
	void initAltitudesAndVerticalAngles();

	template<typename T>
	void initList(const T& minValue, const T& maxValue, const T& interval, vector<T>& list) const;

	vector<float> _normalizedAltitudes;
	vector<float> _normalizedVerticalAngles;
	float _normalizedEarthRadius;

	// Simulated ratio: Chapter 16. Accurate Atmospheric Scattering. https://developer.nvidia.com/gpugems/GPUGems2/gpugems2_chapter16.html
	const float ratioOfEarthRadiusToAtmosphericThickness = 40.0;
};

#endif // !GLOBALCONSTANTS_H
